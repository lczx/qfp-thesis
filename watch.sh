#!/bin/bash
shopt -s globstar
cd $(dirname $0)
make pdf
while inotifywait -qqe move_self **/*.tex **/*.svg **/*.png **/*.bib; do
    make pdf
done
