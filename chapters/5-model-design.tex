\chapter{Model Design} \label{cha:model-design}

The primary purpose of this thesis work is to demonstrate the usefulness of machine
learning techniques to identify the websites visited by a user by monitoring encrypted
network traffic and to observe whether the presence of a mix of QUIC and \acs{TCP} with
\acs{TLS} traffic could negatively impact the performance of the model.

% The act of leveraging packet sequence information extracted from web traffic to identify
% the web activity of a user is known as \emph{website fingerprinting} attack. Such an
% attack is able to break the privacy expected from encrypted communication and other
% privacy enhancing technologies like Tor or \ac{VPN} protocols.

After an initial overview of the methodological approach proposed to design a model for
website fingerprinting, this chapter will describe in detail the steps of the methodology
and analyze the possible solutions.

\newpage

\section{Methodological Approach} \label{sec:methodological-approach}

% The one we are dealing with is a \textit{multiclass classification} problem: our
% objective is to predict the site visited by a user among a selection of monitored
% websites with good accuracy. This site selection are the \textit{classes} (or
% \textit{labels}) among which the model has to choose when classifying previously unseen
% data. In our model, data consists of lists of packets collected from the network (i.e.,
% \emph{traffic samples}) or characteristics extracted from such samples called
% \emph{features}.

The methodological approach proposed for website fingerprinting is formulated as a
supervised learning problem. \prettyref{fig:workflow} shows the sequence of steps of the
methodology.

\begin{figure}[h!]
  \vspace{0.1cm}
  \centering
  \includesvg[width=\textwidth,inkscapelatex=false]{workflow}
  \caption{Methodological approach workflow}
  \label{fig:workflow}
\end{figure}

The first step consists in the label selection, that is, the identification of a set of
website domains to be used as labels in classification. The definition of labels is
mandatory for the creation of a dataset for supervised learning and determines the
websites that the model should be able to recognize from network traffic.
% This selection may discard websites with low availability to ease the subsequent data
% acquisition step. Traffic samples of connections made to these websites have to be
% collected to be used as training data for the model, so \acp{URL} need to be chosen to
% perform network requests and start acquiring data.
The data acquisition step refer to the collection of such traffic; the packets being
collected need to be checked and cleaned in the data processing step to discard anomalies
not related to the target website.

Feature engineering deals with the extraction of the features describing the collected
traffic and with the evaluation of different feature selection and conversion strategies.
The classification step refers to the training of a predictor over the selected features.

% The data processing, feature engineering and classification steps all follow an
% experimental approach: performance gains and losses resulting from the decisions made in
% these steps are measured in a final evaluation process used to assess how the resulting
% models are able to classify new data. This is followed by an analysis of the obtained
% results.

Details of each step of the methodology are presented the following sections.

\section{Label Selection} \label{sec:label-selection}

Building a dataset for supervised learning requires the choice of labels according to the
classification objectives. In this study, the objective is to design a model able to
classify traffic samples as visits to one of the website labels in a well-defined set.
Websites in this set are required to provide \acs{HTTP}/3.0 support (based on QUIC) so to
evaluate whether QUIC traffic could hinder the performance of a website fingerprinting
model.

The selection of the labels depends on whether \emph{closed-} or \emph{open-world}
scenarios (see \prettyref{sec:sota-fingerprinting}) are considered. While a closed-world
setting only requires labels for the websites we want to use a classification target (the
monitored set), an open-world scenario needs samples from a larger selection of websites
(the unmonitored set) as representative of traffic resulting from navigation outside the
monitored ones.

Labels refer to the \emph{main domain} specified in the \ac{URL} of the home page of the
website, that is, the concatenation of the \ac{TLD} with the second-level domain.
\prettyref{fig:domain-parts} shows a \ac{URL}, which is composed by a \emph{scheme}
indicating the protocol, a server \emph{hostname} and a \emph{path} used by the protocol
to locate a resource on the server.

\begin{figure}[h!]
  \vspace{1mm}
  \centering
  \includesvg[width=110mm,inkscapelatex=false]{domain}
  \caption{Example of \acs{URL} and domain structures}
  \label{fig:domain-parts}
\end{figure}

It is common for the hostname to be a \ac{FQDN} to be resolved into an IP address using
\ac{DNS}~\cite{rfc1034, rfc1035}. The \ac{FQDN} is composed by fields separated by dots
used to locate a node in the \ac{DNS} hierarchy. Read from right to left, the \ac{TLD} and
the second-level domain are enough to identify the \ac{DNS} zone of a website, with
additional subdomains commonly used to separate resources under the same site.

The choice of the main domain as a label is motivated by the ability to group under a
given label traffic samples collected from different subdomains and different paths. The
resulting dataset can be used for training a model that is more robust and has higher
probability to correctly classify new traffic.
% vv NO vv
% generated in situations where, for example, the user writes a \ac{URL} in the browser's
% address bar without specifying the \texttt{www} subdomain widely used for websites.

% Unlike attackers, who would likely have a list of websites to monitor...
As initial sources of labels, public domain lists can be used, such as the Alexa Top 1M
list of most popular
websites\footnote{\url{http://s3.amazonaws.com/alexa-static/top-1m.csv.zip}}, the Cisco
Umbrella top 1M list of frequently queried \acs{DNS}
domains\footnote{\url{http://s3-us-west-1.amazonaws.com/umbrella-static/index.html}} and
the Majestic Million list of top 1M most referred
domains\footnote{\url{https://majestic.com/reports/majestic-million}}.

As shown in \prettyref{fig:label-pipeline}, the public list is processed through a label
selection pipeline composed of two filters and a \ac{URL} selection step. Filters have the
purpose of removing duplicated websites as well as unreachable hosts or hosts not
supporting QUIC.
% This is followed by a selection of \acp{URL} for each label to be used in data
% acquisition.

\begin{figure}[h!]
  \vspace{2mm}
  \includesvg[width=\textwidth,inkscapelatex=false]{pipeline-label}
  \caption{Label selection pipeline}
  \label{fig:label-pipeline}
\end{figure}

\subsection{Domain filtering}

To increase the variability of
the labels and the corresponding dataset, domain filtering aims at identifying multiple
versions of a given website hosted under different \acp{TLD} (e.g., \texttt{google.com},
\texttt{google.it}) and at discarding all versions but one, that is the most widely known.

Domain filtering also aims at identifying web servers with support for QUIC. The
negotiation between \acs{HTTP}/1.1 and \acs{HTTP}/2.0 is based on \acl{ALPN} provided by
\ac{TLS}. However, \acs{HTTP}/3.0 over QUIC can only be used for subsequent \ac{UDP}
connections if a server indicates \acs{HTTP}/3.0 support through the \ac{HTTP}
\texttt{Alt-Svc} header
% \cite{rfc7838}
after \ac{TCP} and \ac{TLS} connections have been established. This knowledge can be used
to discard websites that do not have QUIC capability.

% In this step, regular HTTPS connections are performed to each website to observe the
% value of their response's optional \texttt{Alt-Svc} header. Another connection is then
% established to sites signaling support for QUIC over \acs{HTTP}/3.0 to check for
% effective service availability --- if this fails, the site is discarded.

\subsection{Selection of \acsp{URL}}

As previously pointed out, multiple \acp{URL} can be chosen for a website thanks to the
use of the \emph{main domain} as label. Potential \ac{HTTP} redirections can be exploited
to populate the list of \acp{URL} of each website.
% Connecting to a \ac{URL} consisting only of the main domain of a website might cause the
% server to answer with one or more redirects to different \acp{URL} before returning the
% requested content. This chain of redirects can be used to systematically gather a list
% of \acp{URL} for a large number of websites.
As an example, the \ac{URL} \url{https://eff.org/} might redirect to
\url{https://www.eff.org/}, thus providing two variants for the same label with slightly
different traffic patterns.

Using chains of redirects as a \ac{URL} source poses various issues. For example, the
variable length of a chain may lead to an unbalanced dataset where some labels are more
represented than others.
% Care should be taken when generating lists of \acp{URL} for the data acquisition step so
% to have a balanced dataset allowing a correct measure of accuracy during model
% evaluation, as discussed in \prettyref{sec:ml-evaluation}.

% Once these issues have been solved, labels can be split in monitored and unmonitored
% sets to model both open- and closed-world scenarios.

\section{Data Acquisition} \label{sec:data-acquisition}

Data acquisition is a step required to collect a dataset used to train and evaluate the
proposed machine learning model. After identifying a set of websites to be used as labels,
data acquisition consists in performing web requests to the chosen websites and collecting
the generated network traffic. This results in a dataset consisting in a set of traffic
samples labeled with the names of the visited sites.
% The collection of such a dataset requires care in designing a data gathering setup and
% subsequent steps for preprocessing and cleanup.

Our goal is to distinguish between visits to different websites as done by the user of a
web browser. A web page consists of multiple static resources (e.g., scripts, images) and
dynamic content. To emulate real navigation patterns, an automated browser can be used to
load resources in the same way a user would and then measure the resulting traffic.
% Studies have shown that network traffic generated by scripts can exhibit different
% patterns than human-generated traffic~\cite{rezaei2018achieve, rezaei2019deep}.

% It is of paramount importance to collect a quality dataset to avoid drawing misleading
% conclusions in data analysis, feature selection~\cite{rezaei2019deep} and avoid
% hindering the achievable performance of the resulting model.

% With the aim to generalize the machine learning model as much as possible, traffic
% samples taken using different environments and configurations can be considered.
The choice of the browser, operating system and network conditions can affect the
resulting measurements. Additionally, the use of caches by web browsers has a great
influence over the traffic patterns of subsequent connections to the same website, and can
be exploited as an additional effort towards modeling more realistic web traffic.

Other software in execution on the device used to perform measurements can interfere with
the collection of network traffic. To prevent that, data acquisition should be performed
in an isolated environment where traffic samples can be acquired from a properly
configured virtual interface only used by the browser.

Collected traffic can be subject to sanity checks at acquisition time to discard samples
without server traffic or containing remnants of preceding connections while performing
multiple measurements in a row. Tweaking the time interval between subsequent acquisitions
can be another solution to mitigate contamination among traffic samples.

\section{Traffic Preprocessing} \label{sec:capture-preprocessing}

Traffic preprocessing refers to the identification and removal of anomalies in the
collected data.

A type of anomaly consists of packets in a traffic sample not containing data related
to the chosen website. These anomalies can be due to applications running on the machine
used to collect the network traffic (as already discussed) or caused by the browser
itself. In fact, browsers contact different vendor-provided services for various purposes,
such as preferences synchronization, geolocation, support for Web Push Notifications,
download of content blocking signature definitions.

This type of traffic can be identified by performing an analysis of the collected data in
the search for hosts that receive connections in the majority of uncorrelated traffic
samples. These hostnames need to be resolved to IP addresses so to be able to discard
traffic unrelated to the websites of our interest; addresses can change often, and care
must be taken in choosing the time at which hostnames are resolved.

An alternative approach consists of simply dropping all traffic not directed to the
resolved IP address of the \ac{URL} used to collect a sample. However, since sites usually
load resources served from different domains, this would discard a large amount of traffic
that could be useful for fingerprinting.

% ...resolving a list of hosts at a later time might return a different set of IPs than
% the ones visible in the traffic sample  --- resulting in an ineffective
% filtering step. % !! no "filtering"?
% Performing an analysis of the \ac{DNS} packets present in samples is a better approach,
% allowing us to filter out packets containing the address of undesired hosts resolved at
% acquisition time.

The preprocessing also includes a trivial check for errors at the transport layer in the
first packets of each traffic sample to detect connectivity issues or generic server
errors.
% ...over HTTPS port 443 is a \ac{TCP} \texttt{SYN} or a \ac{UDP} datagram from the
% browser to any destination (permanent redirects could influence the remote host being
% contacted when the browser cache is in use) and that the second packet in the same
% conversation is an appropriate answer from the same host.
More complex analysis of the encrypted content transferred by \ac{TLS} or QUIC could be
performed by dumping encryption secrets from the browser and using them to inspect the
underlying \ac{HTTP} traffic for checking the response codes and content served by the
website. Traffic samples containing errors are dropped and collected again.

The proposed model is to be trained over encrypted HTTPS traffic. Given that the port
number assigned by \ac{IANA} for this kind of traffic is 443, % :cite{ianaportdb}
flows with a different server port number are simply ignored.

\section{Feature Engineering} \label{sec:feature-engineering}

\emph{Feature engineering} refers to the processing of features extracted
from the collected data. Feature engineering is a crucial task since an incorrect
selection of features can lead to poor classification performance.

% This work deals with feature engineering by pursuing an experimental approach:
% for each step, practical guidelines are followed to make a selection of strategies
% which are then evaluated in the search for the best classification results.

In general, feature extraction is followed by % optional steps of
feature selection, normalization and dimensionality reduction, as explained in what
follows.

\subsection{Feature extraction}

% Previous
% research~\cite{smith2021website, li2018measuring, wang2014effective,
% shi2009fingerprinting, panchenko2011website, panchenko2016website} on website
% fingerprinting has effectively used various groups of features such as packet counts,
% packet transposition, and distribution; as will be discussed later on.

% Previous research on website fingerprinting was able to obtain good accuracy using
% different sets of features. Part of the purpose of this work is to see if the same
% features allow also to obtain good results on mixed QUIC and \acs{TCP} with \acs{TLS}
% traffic.

The only information available from encrypted traffic is packet timestamp, size, and
protocol. Feature extraction consists in using domain-specific knowledge to convert this
information into metrics useful for the classification. Features can be grouped according
to the properties they refer to, namely:

\begin{itemize}

  \item \textbf{Packet count} features refer to the incoming, outgoing and total number
    of packets, and to the ratios between incoming and total packets and between outgoing
    and total packets;

  \item \textbf{Time statistics} features refer to the total elapsed time, maximum, mean,
    standard deviation and 3\textsuperscript{rd} quartile of the packets inter-arrival
    time, median, 1\textsuperscript{st} and 3\textsuperscript{rd} quartiles of packet time
    relative to the first packet in the traffic sample;

  %\cite{wang2014effective, hayes2016k}
  \item \textbf{Transposition} features refer to the total number of packets transmitted
    before each of the first $n$ (e.g., 300) incoming and outgoing packets;

  %\cite{wang2014effective}
  \item \textbf{Interval} features refer to the number of packets in the first $n$ (e.g.,
    300) incoming and outgoing windows (i.e., series of consecutive packets) between a
    packet and the previous packet in the same direction;

  %\cite{shi2009fingerprinting}
  \item \textbf{Interval frequency} features refer to the frequency of intervals with a
    number of packets in a given range, e.g., [0--300];
    %that is a vector $V$ in which $V(i)$ records the number of intervals with packet
    %number $i$. Intervals with more than 300 packets are counted as having 300 packets;

  %\cite{panchenko2011website}
  \item \textbf{Accumulated interval frequency} features refer to the sum of ranges of
    features (e.g., 3\textsuperscript{rd} to 5\textsuperscript{th}, 6\textsuperscript{th}
    to 8\textsuperscript{th}, and 9\textsuperscript{th} to 11\textsuperscript{th}) from
    the interval frequencies group;

  %\cite{wang2014effective}
  \item \textbf{Burst} features refer to the total number of bursts (i.e., windows of
    packets in the sequence of outgoing packets without two adjacent incoming packets),
    the number of bursts with more than a given number of packets (e.g., 5, 10 and 20),
    maximum and average number of packets in each burst;

  %\cite{wang2014effective, hayes2016k}
  \item \textbf{Packet distribution} features refer to the numbers of outgoing packets in
    the first $n$ non-overlapping groups of $m$ packets (e.g., $n = 200$, $m = 30$), the
    standard deviation, mean, median and maximum, as well as the sum of % 20
    evenly-sized subsets of this array;
    % (the vector is zero-padded in case of samples having less than 200 chunks)

  \item \textbf{First and Last packet information} features refer to packet sizes,
    direction of the first $n$ packets of a sample, and the number of incoming and
    outgoing packets in the first and last $m$ packets of the collected traffic (e.g., $n
    = 20$, $m = 30$);
    % to measure the information given by the first and last packets of a sample,

  \item \textbf{Packet rate} features refer to the number of packets transferred per
    second for the first $n$ seconds (e.g., 100), to their mean, median, minimum, maximum
    and standard deviation, as well as their sum into % 20
    evenly sized sets;

  %\cite{panchenko2016website}
  \item \textbf{Interpolated cumulative sum} features refer to samples taken from the
    piecewise linear interpolant of the packet sizes cumulative sum.

\end{itemize}

% Parameters for these groups of features have been taken from the literature, however a
% model implementation may take in consideration altering these values to observe the
% effect over classification performance.

Other possible features refer to the source and destination IP of the packets, however
since more and more websites are hosted by cloud providers nowadays, IP addresses tend to
change frequently, and these changes could hinder classification performance.

% TODO dataset flavors - doesn't belong here
% Different models can be trained using features extracted from a subset of packets in the
% original traffic samples. By excluding small packets from a traffic sample, for example,
% we can discard a large part of control information from the flow to observe if this can
% improve classification accuracy. On the other hand, by considering only the first
% packets of a sample, we can evaluate model performance in an \emph{online classification
% scenario}~\cite{rezaei2019deep}. In online classification, the flow needs to be labeled
% within the first few packets, before the connection is closed.

\subsection{Feature selection, scaling and dimensionality reduction}

Feature selection consists of applying techniques to identify the most important features
and discard the remaining ones in an effort to obtain a better and leaner
model. As discussed in \prettyref{sec:ml-featsel}, \emph{filter}, \emph{wrapper} and
\emph{embedded} methods can be used for this purpose.

% A multiclass classification problem prevents us from using trivial correlation-based
% methods since the correlation between a feature and an (unsorted) output label set
% cannot be measured.

% It is expected for filter methods to perform worse than wrapper or embedded methods.
% Filter methods use a univariate metric to rank single features, making them unable to
% detect good feature combinations that can be identified by the other approaches.

Feature scaling is an optional step that can be applied to improve the performance of
classifiers. Among the different approaches described in
\prettyref{sec:ml-normalization}, usually scaling is obtained through standardization,
that is, scaling all the features to zero mean and unit variance.

% Scaling is also required to perform...
\emph{Dimensionality reduction} is often applied to obtain a small set of uncorrelated
components from a large number of features. A commonly used approach is \emph{\acl{PCA}}.
A key point is the choice of the number of components to be retained in order to preserve
most of the information from the original features; the techniques that can be applied for
this purpose are described in \prettyref{sec:ml-dimred}.

\section{Classifier Choice}

To undertake the closed-world scenario, classifiers have to handle multiclass
classification problems. Simple algorithms like Logistic Regression and \acl{KNN} can be
applied; more sophisticated algorithms like \acf{SVM} and Random Forests are then used in
search for better classification performance at the cost of an increased computational
effort.

While the training phase of \ac{SVM} can be slow, its kernel function can be changed to
make it able to handle features non-linear with respect to the classification target.
Random Forests and other tree ensembles do not expect linear interactions in data, and
can handle large feature spaces and numerous training samples with relatively few tuning
parameters.

% This work did not consider Multinomial Na\"ive Bayes despite being a simple and fast
% algorithm that works well with text classification and spam detection. The na\"ive
% assumption of feature independence against the heterogeneous feature sets typical of
% website fingerprinting scenarios do not make it an attractive choice as a classifier.

For the open-world setting, the model not only has to discriminate among labels in
the monitored set, but should also be able to classify traffic falling outside this set.
While the predictors stated above could still be used (by e.g., grouping samples of sites
outside the monitored set under a common label), research in website fingerprinting
brought to the development of classifiers designed specifically for this task like
$k$-fingerprinting and Var-CNN, described in \prettyref{sec:sota-fingerprinting}.
% Classifiers taken from website fingerprinting literature are used in this case and
% compared over the data collected in this work.

\section{Model Evaluation} \label{sec:ch5-eval}

% A procedure needs to be defined to evaluate all the tested models in the same way using
% the techniques described in \prettyref{sec:ml-evaluation}.

Model evaluation refers to the task of performing measurements to assess the quality of a
model. This is an essential step to compare the different design choices that can be taken
in the data preprocessing and feature engineering steps, as well as in the choice of a
classifier.

To evaluate a model, the collected dataset is split in training and test sets: the
training set is used to fit the classification algorithm, whose performance is then
measured over samples taken from the test set using metrics from
\prettyref{sec:ml-evaluation}.

Different hyperparameter choices are also compared over the training set by using
\emph{k}-Fold Cross-Validation. As a strategy to select model parameters for testing, Grid
search is applied to fully explore small parameter spaces, while Bayesian optimization is
considered to converge towards good hyperparameters whenever Grid search would take too
much time to complete. Once the best parameters are identified, the classifier is trained
on the whole training data and its accuracy measured on the test set.
The advantage of this evaluation setup is to prevent the parameter selection phase from
being dependent on the test dataset, which should only be used to provide a final
evaluation of the tuned model.

Labels should be equally represented in training and test sets, so care must be taken when
splitting the dataset and when performing cross-validation. In particular, the open-world
setting is inherently unbalanced with the samples belonging to the unmonitored set
gathered under the same \texttt{unmonitored} label.

% In an unbalanced set, accuracy is replaced by precision and recall to provide a final
% evaluation, as other metrics from website fingerprinting literature can be used to
% compare the obtained results with existing research. This is followed by an analysis of
% the obtained results.

\newpage

In open-world website fingerprinting, traffic samples are defined as ``positive'' when
they are considered part of the monitored set and ``negative'' when navigation is assumed
to fall outside this set. Positive websites need also to be correctly labeled. This work
uses the definition of positives given by \citeauthor{wang2020high}~\cite{wang2020high},
since it has been shown to be a good measure of classifier performance, namely:

\begin{itemize}

  \item \emph{True positives} are samples belonging to the monitored set that are
    correctly labeled;

  \item \emph{Wrong positives} are samples belonging to the monitored set that are
    incorrectly labeled;

  \item \emph{False positives} are samples of sites not part of the monitored set but are
    incorrectly labeled as monitored websites.

\end{itemize}

\acs{TPR}, \acs{WPR} and \acs{FPR} are \emph{true}, \emph{wrong} and \emph{false positive
rates}, respectively. These rates are defined as follows:

$$
  TPR = \frac{\textit{\# of true positives}}{\textit{\# of all positives}}
  \qquad
  WPR = \frac{\textit{\# of wrong positives}}{\textit{\# of all positives}}
$$

$$FPR = \frac{\textit{\# of false positives}}{\textit{\# of all negatives}}$$

% ?? As a consequence, recall is equivalent to the \acl{TPR} as the rate of labelling a
% monitored web page as the correct one.

An open-world model should be considered effective only if its positive classifications
are largely correct. \acs{TPR}, \acs{WPR} and \acs{FPR} alone cannot be used to determine
the effectiveness of a classifier in real-world scenarios where the rate $r$ of hitting a
website in the monitored set can be relatively low. \emph{r}-precision is a formulation of
the precision metric that can be used to take this into consideration, it is defined as:

$$\pi_r = \frac{TPR}{TPR + WPR + r \cdot FPR}$$
