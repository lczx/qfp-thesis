\chapter{State of the Art} \label{cha:sota}

QUIC is a relatively new protocol and current research efforts are focusing on both its
security and performance with different configurations under different scenarios.
Extensive work has also been done in the field of network traffic fingerprinting, where
machine learning and deep learning techniques are used to classify protocols,
applications, websites and different types of traffic. %~\cite{rezaei2019deep}

This chapter aims to be a survey of the state of the art of the topics discussed in this
thesis work.
%, that is, application of website fingerprinting techniques to QUIC.

\newpage

\section{QUIC research}

Numerous works have been published prior to QUIC standardization. Considering the large
amount of changes since the first Google GQUIC implementations, these works had to be
scrutinized so to discard outdated information, especially from the security point of
view.

\subsection{Security}

Given that it is still common for new protocols to be \emph{first deployed and analyzed
later}, academic work is focusing on bridging the gap by deploying provable security
models~\cite{lychev2015secure, delignat2020security} against strong adversaries, primarily
finding attacks that do not affect confidentiality and integrity of the protocol but
rather its availability, hitting against the mechanisms designed to reduce latency ---
being in the form of Denial of Service (DoS) attacks or fallback to \ac{TCP} and
\ac{TLS}~\cite{soni2021security}.

Furthermore, significant changes to the standard since Google's previous drafts made it so
that little of the early analysis~\cite{fischlin2014multi, lychev2015secure} applies to
the \ac{IETF} version~\cite{delignat2020security}. This also resulted in the final
\acp{RFC} to include a comprehensive assessment of QUIC's security as discussed in
\prettyref{sec:quic-security-cons}. Some features present in Google's variant of the
protocol have been explicitly removed from the \ac{IETF} standard due to uncertainty in
their security implications, examples are transport protocol compression (which resulted
in CRIME and BREACH attacks in previous versions of \ac{TLS})~\cite{delignat2020security}
and forward error correction~\cite{soni2021security}. %\cite{haxx-quicv2}

An explicit goal of the QUIC working group was for the protocol to inherit the proven
properties of \ac{TLS} 1.3. Standing to \textcite{delignat2020security}, the group has
failed to achieve such goal, with the latest drafts progressively opening up many internal
\ac{TLS} abstractions and diverging from the context under which \ac{TLS} 1.3 is proven to
be secure. This brought \citeauthor{delignat2020security} to create a formal security
model of \acs{IETF} QUIC as long as a secure implementation of the protocol respecting
this model. While they achieved good performance (2 GB/s) on the record layer, their work
turned out to be $20\%$ slower than their selected unverified baseline,
\texttt{ngtcp2}\footnote{\url{https://github.com/ngtcp2/ngtcp2}}.

\subsection{Performance}

QUIC's performance improvement with respect to \acs{TCP} with \acs{TLS} has been backed by
practical results. \textcite{wolsing2019performance} did a comparison of QUIC's
performance against fine-tuned \ac{TCP} stacks. Their objective was to show that tuning of
TCP parameters can shorten its performance gap when respect to QUIC. They used the
Mahimahi framework~\cite{netravali2015mahimahi} to replicate real world, multi-hosted
websites in a testbed; allowing them to experiment with latency and random packet loss.
For the measurements, instead of only reasoning in terms of Page Load Time (PLT), they
also made use of state-of-the-art visual metrics to better reflect user-perceived speed on
loading web pages. The results showed QUIC outperforming \ac{TCP} almost in every
scenario, primarily thanks to its reduced RTT design, stream multiplexing, and, in case of
lossy networks also its ability to circumvent head-of-line-blocking.

\textcite{cicconetti2021preliminary} have done work in replacing the traditional \acs{TCP}
stack with QUIC and \acs{HTTP}/3 in a serverless edge environment to observe whether the
new protocol could be beneficial in this scenario. The serverless model makes use of the
\acl{FaaS} paradigm: execution is broken down in a chain of elementary stateless steps,
reducing the need of complex migrations upon roaming of users between areas of the
network. Expectations are for QUIC to fit well in this scenario, thanks to its low latency
and support for seamless migration. To evaluate performance, they emulated a network of
edge nodes using Mininet\footnote{\url{https://mininet.org}} and compared QUIC to Google
Remote Procedure Call (gRPC) (based on \acs{TCP} with \acs{HTTP}). Their results shown
that QUIC has similar overhead to gRPC over slow networks, which increases when moving to
faster or larger networks --- still a great achievement considering that gRPC does not
have embedded security like QUIC.

% \todoy{Talk about satellite papers?}

\section{Network Traffic Fingerprinting} \label{sec:sota-fingerprinting}

% \todog{If we have time, describe other papers found and used during research} NO

An extensive amount of content can be found regarding the overall classification of
network traffic.

Research groups have posed themselves different goals (from intrusion and malware
detection to resource usage planning, billing systems, and content blocking) which did
influence their efforts towards online or offline classification. While classifying
\emph{offline} usually does not pose time or resource constraints at inference time,
\emph{online} classification has to be performed as quickly as
possible~\cite{rezaei2019deep}, requiring careful selection of features to avoid excessive
computation on fast network flows. Goals do also influence the selection of classes, of
which examples can be protocols (e.g., \acs{HTTP}, SSH, BitTorrent, ...), applications
(video streaming, chat, video games, ...), websites, and so on.

The oldest approach to traffic classification among different protocols was to classify by
using the port numbers extracted from the \ac{TCP} or \ac{UDP} headers of packets and
comparing them to the \ac{IANA} \acs{TCP}/\acs{UDP} port number
database\footnote{\url{https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml}}.
This method is fast, but port forwarding, obfuscation, \acsp{NAT}, and random port
assignments do hinder its accuracy.

Another technique inspects the application layer payload of packets: deep packet
inspection (DPI) methods use patterns such as regular expressions for classification. This
approach provides better results than the port-based one but requires continuous updating
of a signature database. While the widespread use of encryption
today\footnote{\url{https://transparencyreport.google.com/https/overview}} (which would
ideally remove statistical properties from data) may seem to make this method useless,
machine learning or neural classifiers may learn patterns originating from the
pseudo-random number generators backing crypto schemes, still allowing separation of
applications in some scenarios~\cite{lotfollahi2020deep}.

Statistical methods extract time-series features (such as length and inter-arrival time of
packets), calculate derived statistical features, and usually employ a machine learning
approach for classification. Research has shown good accuracy results in different
scenarios; however, feature selection and extraction are critical for success and still
have to be done manually by an expert.

Research made successful use of \ac{SVM}~\cite{cortes1995support},
\acl{KNN}~\cite{altman1992introduction} and Random Forests~\cite{breiman2001random} for
classification of network traffic.

\subsection{Website Fingerprinting}

A variety of work exists related to the identification of a user's activity on the web by
leveraging information gathered from the raw encrypted packet sequences transmitted on the
wire.

%\cite{perrycritique}
Two types of models are used in website fingerprinting. In a \emph{closed-world scenario}
the user is supposed to navigate only between a predefined selection of websites. This
setting has been criticized for being unrealistic~\cite{hayes2016k, juarez2014critical},
bringing more recent research to consider \emph{open-world scenarios} which take in
consideration the ability of a client to visit sites outside a given monitored set.

% When a client visits a web page, an attacker can collect encrypted traffic and attempt
% classification among one of the \emph{n} monitored sites. On the other hand, an
% \emph{open-world setting} allows a client to visit sites outside the monitored set,
% resulting in a more realistic scenario.

Fingerprinting is genuine threat to web privacy even while using software to enable
anonymous communication like Tor, where packet length are hidden by fixed-size Tor cells.
Regardless of the defense scheme in place, \textciteyear{cai2012touching} was able to set
up two successful experiments. The first is a closed-world scenario where a model is
trained to distinguish among 100 web pages. They used \ac{SVM} with a kernel based on
Damerau–Levenshtein edit distance, with the distance function being normalized to take in
account the length of traffic traces. They were able to recognize a web page almost $80\%$
of the time. The second open-world experiment builds two models to detect visits to IMDB
and Facebook by constructing a Hidden Markov Model on the site pages, achieving zero false
positives~and negatives over Facebook and $7.9\%$ \ac{FPR}, $5.6\%$ \ac{FNR} on IMDB.

\textciteyear{wang2014effective} extended over the work of \textcite{cai2012touching,
wang2013improved} by creating an open-world model to distinguish between 100 monitored
sites from non-monitored ones. They exploited multi-modality of web page content (the
variation of network conditions, random page content, page updates over time, and client
configuration) to train a \acl{KNN}~\cite{altman1992introduction} classifier.
\citeauthor{wang2014effective} used features extracted from packet intervals (used by
\textcite{cai2012touching} to achieve high accuracies), transposition and bursts to
achieve a \ac{TPR} of $85\%$ over Tor traffic. For \textcite{panchenko2016website,
hayes2016k}, this is the best \ac{KNN} classifier as of 2016.

\textciteyear{panchenko2016website} propose a novel attack on Tor based on the idea to
sample features from a cumulative representation of a trace; proving that their new
approach CUMUL, the interpolation of the cumulative sum of packet sizes, is more accurate
and efficient than previous state of the art. To compare performance with prior art, they
tested their model over datasets provided by \citeauthor{wang2014effective}
\cite{wang2014effective, wang2013improved}, achieving $96.6\%$ \ac{TPR} ($7\%$ more than
\ac{KNN}) and $9.6\%$ \ac{FPR} ($1\%$ less). \citeauthor{panchenko2016website} also
collected a new comprehensive dataset consisting of different pages for each site and
demonstrated this approach to be more feasible for website fingerprinting at internet
scale.

\textciteyear{hayes2016k}, introduce a new model for open-world website fingerprinting
called \emph{k}-fingerprinting. This classifier extracts a fingerprint from each traffic
sample, that is a vector of leaf identifiers for each tree in a Random Forest. The
classification result is given by a \ac{KNN} model using a Hamming distance function. New
samples are considered part of the monitored set only if all the neighbors agree. They
proved this new model to be more accurate and efficient than both \ac{KNN} and CUMUL when
tested over 30 Tor hidden services with an open-world size of 100,000 unmonitored web
traces, achieving $85\%$ \ac{TPR} and a \ac{FPR} rate as low as $0.02\%$.

\subsection{Deep Learning for Traffic and Service classification}

\textciteyear{lopez2017network} were the first to apply \ac{CNN} and \ac{RNN} models to
network traffic classification to distinguish among 108 different services. \acp{CNN} are
used in image processing to exploit the correlation between surrounding pixels; the
research group's intuition was that neighboring packets in a network flow might have a
similar relationship. Different combinations of the two network models have been compared
over 260+ thousand flows from the Spanish academic and research network (RedIRIS). Best
results were obtained by feeding the results of a \ac{CNN} to a 2-layer \ac{LSTM} \ac{RNN}
with two final fully-connected layers. They also observed that the initial part of a flow
is more important for classification: the first 5--15 packets are enough to achieve good
detection results.

\textciteyear{chen2017seq2img} used a novel approach to online classification using
Reproducing Kernel Hilbert Space (RKHS) embeddings to create multichannel images. These
images are passed to a \ac{CNN} for classification of two datasets: the first one
consisting of five protocols, and the second of five well-known internet applications.
Their model is compared with \ac{SVM}, \ac{MLP}, Na\"ive Bayes and Decision Trees:
classical models requiring manual extraction of features; chosen to be packet size,
inter-arrival time, forward-backward ratios and server IP address. Accuracy was evaluated
with 10-fold cross-validation, with their model achieving an average $99.7\%$ accuracy on
the protocol dataset and $88.4\%$ on the application set, with significant improvement
over the compared methods.

%\cite{chollet2015keras} \cite{tensorflow2015-whitepaper}
\textciteyear{lotfollahi2020deep} proposed a deep learning approach for encrypted traffic
classification, integrating feature extraction and classification phases into one system.
The \ac{CNN} and \ac{SAE} network structures are tested in \emph{application
identification} and \emph{traffic characterization} scenarios on the ISCX VPN-NonVPN
dataset, where a distinction between \ac{VPN} and non-\ac{VPN} traffic is also made. The
implementation, done in Keras\footnote{\url{https://keras.io}} with the
TensorFlow\footnote{\url{https://www.tensorflow.org}} backend, achieved over $95\%$
accuracy and recall for both networks in the application identification task, and over
$92\%$ in traffic characterization, with \ac{CNN} performing slightly better than
\ac{SAE}.

Regarding website fingerprinting, \textciteyear{bhat2019var} ideated an attack leveraging
deep learning techniques able to achieve good performance without requiring large amounts
of data. A novelty of their Var-CNN model (based on ResNet, the state of the art for
computer vision) is the ability to both use  features extracted automatically from raw
data as long as given features like packet counts, transmission time, incoming and
outgoing ratios. The model is evaluated in both open and closed-world settings: $0.36\%$
\ac{FPR} ($4\times$ better than prior art) and $98\%$ \ac{TPR} were achieved in the first
setting, while $97.8\%$ accuracy was obtained in a closed world with only 100 traces over
100 monitored websites.

\subsection{Classification of QUIC traffic}

Regarding classification of network traffic involving the QUIC protocol, Random Forest and
\acfp{CNN} were the choice of \textciteyear{tong2018novel} to identify five types of
Google services running over QUIC. Random Forest makes a first distinction between
Hangouts chat and voice calls using flow-based features, and the CNN is then used to
detect the remaining classes by also considering encrypted data for each packet. All
packets in a flow are processed independently, and a majority rule is then applied to
assign a class to the flow. Tong's model achieved over $99\%$ accuracy on test data, but
its selection of features can make it unpractical in classifying large flows.

\textciteyear{rezaei2018achieve} tackled the scarcity of labeled data availability problem
by performing unsupervised training of a \ac{CNN} over time-series features extracted from
different unlabeled data sources. This network is trained to estimate statistical features
of the flows from a sampled selection of packets inside them (the exact calculation of
such statistics would require knowledge of all the packets inside the flow). Their
research introduced three sampling techniques (fixed step, random and incremental) which
could be used either in high bandwidth scenarios or to augment the dataset. They have
shown that good sampling can achieve upper-bound accuracy in different datasets and that
picking packets from arbitrary portions of a flow can be sufficient for classifying
applications. The statistics-inferring network is then used as a feature extractor for a
second classifier, trained in a supervised way over a limited amount of data. They
classified 5 Google services using the QUIC protocol and achieved close to
fully-supervised accuracy by only using 20 labeled flows for each class. As a last result,
they have seen that using an uncorrelated dataset for the first unsupervised training step
can still improve the accuracy of another labeled dataset for the second classifier.

The main advantage of using deep learning techniques in network traffic classification is
automatic feature learning. Deep learning enables to achieve higher accuracy than
classical machine learning models; however, this is challenged by the requirement of the
collection and labeling of large amounts of data. \textciteyear{iliyasu2019semi} proposed
a semi-supervised learning approach using a Deep Convolutional Generative Adversarial
Network (DCGAN)~\cite{goodfellow2014generative, radford2015unsupervised,
salimans2016improved} to generate synthetic data and improve the performance of
classifiers trained on a few labeled samples. Inspired by the work of
\textcite{lopez2017network}, they used a pseudo-image generated from time-series features
of packets in a flow as input to the GAN. The model was evaluated on two different
datasets: a self-collected one containing QUIC traces and the publicly available ISCX
VPN-NonVPN dataset. When only $10\%$ of the dataset was labeled, accuracy was better than
the fully supervised \ac{MLP} and \ac{CNN} models established as a baseline; however, the
QUIC dataset achieved better performance thanks to the fact that it contains flows with a
large number of packets. They concluded that this method would perform well on datasets in
which applications have long-lasting flows, with many packets in every flow.

About QUIC website fingerprinting, \textciteyear{smith2021website} investigated whether
classifiers trained over \acs{TCP} with \acs{TLS} generalize to QUIC traces and if feature
importance changes between these protocols. Tests involved four classifiers tailored for
fingerprinting (\emph{k}-fingerprinting~\cite{hayes2016k}, DF~\cite{sirinam2018deep},
\emph{p}-FP(C)~\cite{oh2017p} and Var-CNN~\cite{bhat2019var}), trained to identify traces
among 100 monitored domains or to recognize unmonitored ones. Results indicated that QUIC
is not more difficult to fingerprint than \ac{TCP}; however, the model needs to be trained
with QUIC data in order to classify it. A classifier not trained for QUIC does not get
penalized by the presence of QUIC traces while detecting \ac{TCP} traffic. The authors of
this research compared two approaches to classification: the first consists of a single
``mixed'' classifier trained on both protocols, while the second uses a distinguisher and
two protocol-specific classifiers. The mixed approach was able to leverage common patterns
between the protocols, obtaining better accuracy results.

\textciteyear{zhan2021website} compared vulnerability to early traffic website
fingerprinting of Google QUIC, \ac{IETF} QUIC, and the traditional \acs{TCP} with
\acs{TLS} stack in a closed network. \texttt{tshark} was used to collect traces of a
Chrome client (controlled by Selenium\footnote{\url{https://www.selenium.dev}}) connecting
to a local server hosting 100 different websites cloned from the Internet. Extracted
features were then grouped into computationally cheap \emph{Simple} features and
\emph{Transfer} features selected from previous effective research on fingerprinting
\ac{TLS}. They also collected results from using only a variable number of packets from
the start of captured flows, thus evaluating fingerprinting effectiveness on early
traffic. Using Random Forest and other machine learning classifiers, they have shown that
most features can be transferred between protocols with similar weights on normal traffic.
On the other hand, early traffic requires tuning due to variations in initial traffic
distribution among the different protocols. They found out that GQUIC and \ac{IETF} QUIC
are easier to fingerprint on early traffic, even on flawed network connections.
