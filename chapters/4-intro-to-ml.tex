\chapter{Introduction to Machine Learning} \label{cha:ml}

The purpose of this chapter is to introduce machine learning techniques that will be used
in this thesis work.

Website fingerprinting research usually follows a \emph{supervised learning} approach,
that is, a branch of machine learning where the task of learning associates input data to
a known value. This is achieved by collecting a set of training data samples along with
their expected outcome and fitting a model to map each sample to its associated value.

Once a model has learned from training data, it can be used to evaluate new samples of
previously unseen data of the same kind.

\newpage

\section{Introductory Concepts}

The nature of the expected value given by a machine learning model determines the name of
the learning task, which is said to be either a \emph{regression} problem, for a
continuous variable, or a \emph{classification} problem, if the outcome is a discrete
class label. In classification problems, choice of the labels depends on the project goal.
Differences between binary (two-label) and multiclass classification problems influence
the selection of a prediction algorithm and the metrics used to evaluate its performance.

% In website fingerprinting, depending on the chosen goal, the label can indicate whether
% its associated data corresponds to a website that is part of a monitored set (binary
% classification) or identify a specific site given a selection of websites chosen as
% labels (multiclass classification).

Data can represent anything, with complex inputs usually requiring a lossy \emph{feature
extraction} step to convert them into a set of \emph{features}. Features are
characteristics of the observed raw data (e.g., brightness or color histograms for an
image) that provide a simpler representation to ease the learning process.

A Deep Learning approach finds patterns in the raw samples and, given enough time and
data, classifies them with good accuracy without a feature extraction step. On the
contrary, traditional machine learning models require a manual selection of features.
These models are easier, faster to train and can achieve results comparable with Deep
Learning methods. In this context, features play a key role.

Feature extraction falls under the machine learning task of \emph{feature engineering}.
With feature engineering we refer to the whole process and techniques used to gather a
meaningful set of features.

After obtaining an initial set of features extracted from the input data, it is good
practice to perform a \emph{feature selection} step to discard features that provide
little to no gain in prediction performance.

\section{Feature Selection} \label{sec:ml-featsel}

A good subset of the original features usually yields models that are faster, more
generalizable and easier to interpret. In addition, fewer features translate into a
reduced risk to overfit training data and into a better accuracy~\cite{dash1997feature}.

Feature selection methods able to handle multiclass classification problems (like the one
of website fingerprinting) can be divided into three groups: \emph{filter}, \emph{wrapper}
and \emph{embedded} methods~\cite{liu2005toward}.

\subsubsection{Filter methods}

Filter methods rank features individually through a univariate metric, providing the
simplest class of feature selection methods. \prettyref{fig:featsel-filter} shows the
feature selection workflow when using filter methods: selection is done in a single step
independently of classification and evaluation, which are then performed over the chosen
reduced set of features.

\begin{figure}[h!]
  \vspace{0.2cm}
  \centering
  \includesvg[width=11cm,inkscapelatex=false]{featsel-filter}
  \caption{Feature selection with filter methods}
  \label{fig:featsel-filter}
\end{figure}

Examples of filter methods are:

\begin{itemize}

  \item \textbf{Variance Threshold} consists of measuring the variance of features in the
    training set and removing the ones whose value is less than a given threshold. A
    Low-variance features are inherently useless for classification;

  % \cite{plackett1983karl}
  \item \textbf{Pearson's Chi-Squared test} is a statistical procedure to determine the
    difference between observed and expected data and can be used to determine the
    correlation between features and categorical labels. Features that are most likely to
    be independent of the class are considered irrelevant for classification;

  % \cite{lee2012novel}
  \item \textbf{Mutual Information} consists of applying \emph{information gain}
    (typically used in the construction of decision trees) by measuring the reduction in
    uncertainty for one feature given a known value of the other feature. It can capture
    any kind of statistical dependence, but being non-parametric, requires more samples
    for accurate estimation.
    % https://machinelearningmastery.com/information-gain-and-mutual-information

\end{itemize}

Although filter methods are model agnostic, it has to be noted that by ranking individual
features they might miss lower-ranked features that could be combined with other features.

\subsubsection{Wrapper methods}

Wrapper feature selection methods perform a greedy search over the features, chosen
through the iterative process of model training and evaluation shown in the workflow
illustrated by \prettyref{fig:featsel-wrapper}. Evaluation results are used to collect a
subset of features so to maximize the accuracy of the model.

\begin{figure}[h!]
  \vspace{0.2cm}
  \centering
  \includesvg[width=11cm,inkscapelatex=false]{featsel-wrapper}
  \caption{Feature selection with wrapper methods}
  \label{fig:featsel-wrapper}
\end{figure}

\newpage

The possible wrapper methods are:

\begin{itemize}

  \item \textbf{Sequential Feature Selection}:
    starting from an empty set, this method adds at each iteration a feature that
    maximizes the model's gain in accuracy;

  % \cite{guyon2002gene}
  \item \textbf{Recursive Feature Elimination}:
    this method removes at each step some features from the full set with the objective of
    retaining most of the accuracy.

\end{itemize}

Given their ability to model dependencies among input features, wrapper methods are
usually preferable with respect to filter approaches; although the search for features is
computationally expensive, and the resulting feature selection depends on the chosen
prediction algorithm.

\subsubsection{Embedded methods}

Embedded methods use the weights given to features by a trained predictor to perform
feature selection. As shown in \prettyref{fig:featsel-embedded}, selection of features and
model fitting are performed in a single step before evaluation.

\begin{figure}[h!]
  \vspace{0.2cm}
  \centering
  \includesvg[width=11cm,inkscapelatex=false]{featsel-embedded}
  \caption{Feature selection with embedded methods}
  \label{fig:featsel-embedded}
\end{figure}

However, it is also possible to train and evaluate a different classifier using the
selected features. Two possible embedded methods for feature selection are:

\begin{itemize}

  \item \textbf{Feature Importance}: a method selecting features by using weights
    extracted from a predictor. The greater is the value of a feature's weight, the more
    the feature is considered to be valuable. Predictors that make use of a loss function
    can add a regularization term to the loss as a penalty parameter to prevent
    overfitting over the training set. The regularization term is based on the norm of the
    weights vector and the choice of the norm function ($L_1$ for \emph{Lasso}, $L_2$ for
    \emph{Ridge} and $L_1$ + $L_2$ for \emph{ElasticNet}) result in different trained
    weights, which can then be used as a source of information for feature selection.
    Notably, $L_1$ regularization yields many weights being zero, thus the corresponding
    features can be discarded;

  \item \textbf{Decision Trees}:
    impurity-based feature importance as taken from decision tree estimators (e.g., Random
    Forests and Gradient Boosting) can be used as a criterion to rank and select the most
    valuable features.

\end{itemize}

Embedded methods combine the advantages and disadvantages of filter and wrapper
approaches: they are fast, accurate, and can take into consideration combinations of
features.

\section{Dimensionality Reduction} \label{sec:ml-dimred}

Dimensionality reduction is an approach applied to reduce the number of features to a
machine learning model. In this context, the most popular algorithm is
\acf{PCA}~\cite{pearson1901liii}.

\ac{PCA} computes the eigenvectors of the features' covariance matrix (i.e., the principal
components) where the $i$-th vector is the direction of a line that best fits the data
while being orthogonal to the previous $i - 1$ vectors.

% The best component vectors are the ones that minimize the average squared distance of
% input feature samples to the line traced by the vector.

% These vectors are used to perform a change of basis on the data, resulting in a list of
% independent components that can take the place of the original features in a machine
% learning model.

In order to perform dimensionality reduction, the components that explain most of the
variance should be kept. Different rules can be applied to choose the number of components
to keep:

\begin{itemize}

  \item \textbf{Kaiser's Rule} consists in dropping components whose eigenvalue is below
    1, a qualitative choice to preserve major components that yield more information than
    the average feature input to \ac{PCA}~\cite{kaiser1960application, yeomans1982guttman,
    zwick1986comparison};

  \item \textbf{Scree Test}~\cite{cattell1966scree} plots the components' eigenvalue in a
    downward curve, ordering the eigenvalues from largest to smallest. The rule specifies
    to keep all components until the resulting \emph{scree plot} makes an elbow towards a
    less steep decline.

  \item \textbf{Cumulative Explained Variance} sorts components by their explained
    variance and keeps only enough factors to account for a percentage between 70\% and
    90\% of the total variance.

\end{itemize}

% \todoy{Should put critiques to these criteria in the evaluation chapter?}

It has to be noted that the \ac{PCA} algorithm relies on features centered around the
origin (i.e., mean equal to zero), thus dimensionality reduction requires \emph{feature
scaling}.

\section{Feature Scaling} \label{sec:ml-normalization}

Feature scaling is applied to convert features into a similar range of values. In fact,
algorithms that compute the Euclidean distance between feature vectors (e.g., \acl{KNN})
may give suboptimal results if this range varies significantly. Feature scaling also
allows algorithms trained through gradient descent to converge faster.

The most widely used feature scaling techniques are:

\begin{itemize}

  \item \textbf{Standardization} consists in converting all features to zero mean and unit
    variance. The feature $\overline{x}$ resulting from standardization of feature $x$ is
    calculated as follows:

    $$\overline{x} = \frac{x - \mu}{\sigma}$$

    Where $\mu$ denotes the expected value and $\sigma$ the standard deviation of the
    feature over a set of samples.

  \item \textbf{Min Max scaling} transforms each feature $x$ in the range of $[0,1]$ as
    follows:

    $$\overline{x} = \frac{x - x_{min}}{x_{max} - x_{min}}$$

    Where $x_{min}$ denotes the minimum and $x_{max}$ the maximum value of the feature
    over the training set.

  \item \textbf{Absolute Maximum scaling} alters each feature $x$ individually as follows:

    $$\overline{x} = \frac{x}{\max(\lvert x \rvert)}$$

    Where $\max(\lvert x \rvert)$ denotes the maximum of the feature's absolute values in
    the training set.
    % ...so that this value is $1.0$ in the transformed set.

  \item \textbf{Robust scaling} converts each feature $x$ using statistics that are robust
    to outliers as follows:

    $$\overline{x} = \frac{x - \mu}{x_{75} - x_{25}}$$

    Where the feature value is first converted to zero mean and then scaled according to
    the interquartile range between the first ($x_{25}$) and the third quartile ($x_{75}$)
    measured over all feature samples.

  \item \textbf{Instance normalization} operates on whole feature vectors and is an
    alternative to feature by feature scaling when all components of the vector represent
    the same kind of information. The scaled feature vector of a training sample is
    calculated as follows:

    $$\overline{x}_i = \frac{x_i}{\lVert \mathbf{x} \rVert}$$

    Where $x_i$ is the $i$-th component of the feature vector $\mathbf{x}$ and $\lVert
    \mathbf{x} \rVert$ is its $L_2$ (Euclidean) or $L_1$ (Manhattan) norm.

  % The ML course also introduces Whitening

\end{itemize}

\section{Predictors} \label{sec:ml-algorithms}

% TODO ref
The core of a prediction model is the classification algorithm whose choice depends on the
nature of the problem: primarily that is, problem complexity, the type of the predicted
variable and (if known) the statistical properties of input features. The prediction
algorithms used in this thesis work are:

\textbf{Logistic Regression} is a discriminative algorithm modeling a logistic model, that
is the probability $p(y|\mathbf{x})$ of class label $y$ being representative of a vector
of features $\mathbf{x}$. Regression itself is the process of estimating the parameters
(i.e., the weights) of the logistic model, this is a maximization problem usually dealt
with by using an optimization algorithm like gradient descent. Logistic Regression is easy
to interpret and fast to train; it is a good baseline model but only well suited for
linear problems. \emph{Multinomial} Logistic Regression is a generalization for multiclass
problems.

% \cite{cortes1995support}
\textbf{\acf{SVM}} is another discriminative algorithm that uses a subset of (support)
training points to perform a separation between the labels. \ac{SVM} is a robust algorithm
able to achieve good performance on small datasets with a clear separation between the
classes. Multiclass \ac{SVM} is obtained by using \emph{one-versus-one} and
\emph{one-versus-rest} decision functions. Both strategies split a multiclass dataset into
multiple binary classification problems that can be fitted with \ac{SVM}, and each model
votes towards the final classification result.

\enlargethispage{-\baselineskip}

\textbf{\acf{KNN}} is a supervised learning method measuring the distance of a feature
vector to be classified to the vectors of samples from the training set. No computation is
done in the training phase. During classification, an unlabeled vector is given the label
that is most frequent among the $k$ training samples nearer to that point. Contributions
can be weighted, with the nearer neighbors contributing more to the result than the more
distant ones.
% \ac{KNN} is able to achieve great results in the website fingerprinting
% scenario~\cite{wang2014effective}.

% \cite{breiman2001random}
\textbf{Random Forests} is a tree ensemble learning algorithm; it makes use of multiple
decision trees (i.e., the forest) composed of nodes connected in a hierarchy through
incoming and outgoing edges. The leaves of a tree do not have outgoing edges and represent
the labels of the classification problem. Random Forests training is based on a bagging
(bootstrap aggregation) algorithm: random selections of samples are taken from the
training set multiple times to train the trees in the ensemble. In addition to bagging,
Random Forests also select a random subset of the features to train each tree, introducing
another element of variability to reduce similarities between the trees. Random Forests
can handle non-linear problems and large feature spaces. For the classification, every
tree votes for a label and the result of the ensemble is the label that receives the
majority of the votes.

\section{Model Evaluation} \label{sec:ml-evaluation}

In order to compare machine learning models and identify the best ones, a methodology and
a set of metrics need to be defined. Evaluation of a model is usually performed over three
sets of data:

\begin{itemize}

  \item \textbf{Training set}, consisting of samples used to train the classifier;

  \item \textbf{Validation set}, consisting of samples used to evaluate and improve the
    model (fitted on the training set) by changing its \textit{hyperparameters}, i.e., the
    configuration variables used by the feature selection and prediction algorithms;

  \item \textbf{Test set}, consisting of samples used to provide a final unbiased
    assessment of the quality of the model.

\end{itemize}

% To achieve good classification results, the setups from where these sets are collected
% have to be as similar as possible, ideally by performing splits of a single master
% dataset. Trained models used in production environments are required to correctly classify
% new data at each use, so care must be taken in collecting and processing fresh data in the
% same way as the dataset used to train the model.

% A separate validation set is used in tuning to avoid having models whose hyperparameter
% choice depends on samples taken from the test set, which should only be used to provide a
% final evaluation of the tuned model.

Assessment of a model during hyperparameter tuning can also be performed by using
cross-validation without defining a separate validation set. Given a model to evaluate,
\emph{k}-Fold Cross-Validation~\cite{kohavi1995study} subdivides a set of samples in $k$
folds. The model is trained $k$ times over $k-1$ folds, using the remaining fold as a
validation set. \prettyref{fig:kfold-cv} illustrates cross-validation with $k = 5$: at
each iteration, a different fold is chosen as a validation set and its accuracy $a_i$ is
computed. The overall accuracy is taken to be the average of the accuracies computed over
all the validation folds.

\begin{figure}[h!]
  \vspace{0.2cm}
  \centering
  \includesvg[width=10cm,inkscapelatex=false]{kfold-cv}
  \vspace{0.2cm}
  \caption{Illustration of \emph{k}-Fold Cross-Validation with 5 folds}
  % \(\displaystyle A = \frac{1}{5} \sum_{i=1}^5 a_i\)
  \label{fig:kfold-cv}
\end{figure}

\subsection{Hyperparameter tuning}

Most feature selection and classification algorithms are parameterized through a set of
hyperparameters that that need to be set before training. In the iterative process of
model tweaking and evaluation, different values of hyperparameters are tested to achieve
maximum performance over the validation set. Different strategies to search the parameters
space (i.e., the range of values of each hyperparameter tested) are applied, namely:

% https://towardsdatascience.com/grid-search-vs-random-search-vs-bayesian-optimization-2e68f57c3c46

\begin{itemize}

  \item \textbf{Grid Search} performs an exhaustive search, evaluating the model over all
    possible parameter combinations. Each parameter must have a finite number of possible
    values. The number of evaluations required increases exponentially with the number of
    parameters, making this approach impractical for large parameter sets;

  %\cite{bergstra2012random}
  \item \textbf{Random Search} evaluates the model over random combinations of
    hyperparameters inside the parameter space. This approach is more efficient than Grid
    Search since it usually requires less computation time to find good parameter
    combinations even though at the risk of missing the best combination of parameters
    possible for the model;

  %\cite{snoek2012practical}
  \item \textbf{Bayesian Optimization} is an informed search method that creates a
    probabilistic model of the parameter space by learning from previous iterations. This
    approach can converge to the best parameter set without trying all possible
    combinations.

\end{itemize}

\subsection{Scoring metrics}

Various metrics can be used to evaluate and compare models. The simplest one is the
\emph{accuracy}, measuring the number of correct predictions over the total number of
predictions:

$$\textit{accuracy} = \frac
  {\textit{\# of correct predictions}}
  {\textit{\# of total predictions}}$$

Accuracy is a trustworthy metric only for balanced datasets, that is, sets of samples in
which all labels are equally represented. When considering unbalanced datasets, a model
can still obtain high accuracy by misclassifying classes with few samples. To evaluate
models over this kind of sets, the terms \emph{\acl{TP}}, \emph{\acl{TN}},
\emph{\acl{FP}} and \emph{\acl{FN}} need to be defined first.

\prettyref{tab:confusion-matrix} shows a $2 \times 2$ \emph{confusion matrix} commonly
used to evaluate binary classification models: in this table, \acfp{TP} and \acfp{TN}
represent the number of samples that are predicted correctly; errors can be either
\acfp{FP}, where the model misclassifies as positive samples that should be labeled as
negative, or \acfp{FN} when negative samples are classified as positive. The sum of these
variables always yields to the total number of samples $N$ in the test set.

\begin{table}[h!]

  \bgroup
  \centering
  \def\arraystretch{1.5}

  % https://tex.stackexchange.com/a/20295
  \begin{tabular}{l|l|c|c|c}
  \multicolumn{2}{c}{}
    & \multicolumn{2}{c}{\itshape Predicted label}
    & \\
  \cline{3-4}
  \multicolumn{2}{c|}{}
    & Positive
    & Negative
    & \multicolumn{1}{c}{Total}\\
  \cline{2-4}
  \multirow{2}{*}{\Gape[6mm]{\makecell[cc]{\itshape Actual \\ \itshape label}}}
    & Positive
    & \makecell{\Acf{TP}}
    & \gape{\makecell{\Acf{FN} \\ {\footnotesize Type-II error}}}
    & $\mathrm{\acs{TP}} + \mathrm{\acs{FN}}$\\
  \cline{2-4}
    & Negative
    & \gape{\makecell{\Acf{FP} \\ {\footnotesize Type-I error}}}
    & \Acf{TN}
    & $\mathrm{\acs{FP}} + \mathrm{\acs{TN}}$\\
  \cline{2-4}
  \multicolumn{1}{c}{}
    & \multicolumn{1}{c}{Total}
    & \multicolumn{1}{c}{$\mathrm{\acs{TP}} + \mathrm{\acs{FP}}$}
    & \multicolumn{    1}{c}{$\mathrm{\acs{FN}} + \mathrm{\acs{TN}}$}
    & \multicolumn{1}{c}{$N$}\\
  \end{tabular}

  \caption{Example of a Confusion Matrix}
  \label{tab:confusion-matrix}
  \egroup

\end{table}

\newpage

The counts defined above are required to compute metrics used to evaluate the performance
of a classification model over an unbalanced dataset, namely:

% TODO
% An unbalanced set might cause a classifier to get biased towards
% making predictions for the more represented classes. Evaluation must also take this in
% consideration, with the accuracy score not being representative of the quality of the
% model (see \prettyref{sec:ml-evaluation}). A strategy can be ideated to collect a balanced
% dataset without recurring to over- and under-sampling of existing data. Furthermore, to
% keep labels representative, we may want to filter out hosts whose final redirect point to
% a different main domain.

\begin{itemize}

  \item \textbf{Precision} (also known as \emph{positive predictive value}) counts the
    correct predictions over all the positive predictions, that is:

    $$\textit{precision} = \frac
      {\textit{\acs{TP}}}
      {\textit{\acs{TP}} + \textit{\acs{FP}}}$$

    A precise model may not find all positive values, but the ones classified as such are
    likely to be correct.

  \item \textbf{Recall} (also known as \emph{sensitivity}) counts the number of positive
    samples a model can find over all the positive ones, it is computed as:

    $$\textit{recall} = \frac
      {\textit{\acs{TP}}}
      {\textit{\acs{TP}} + \textit{\acs{FN}}}$$

    A model with high recall is able to find most of the positives even if it can
    misclassify some negative cases as positive as well.

% \end{itemize}

% Ideally, models with both good precision and recall are preferable, but often we have to
% face a trade-off between them. In many cases, the predictor can be tweaked to favor
% precision over recall and vice-versa. Another metric is used to combine the two into a
% single score:

% \begin{itemize}

  \item \textbf{F\textsubscript{1} Score} is the harmonic mean of precision and recall,
    that is:

    $$F_1 = 2 * \frac
      {\textit{precision} \cdot \textit{recall}}
      {\textit{precision} + \textit{recall}}$$

    The result is a value between zero and one. Models with higher F\textsubscript{1}
    scores have overall good precision and recall.

\end{itemize}
