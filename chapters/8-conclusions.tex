\chapter{Conclusions} \label{cha:conclusions}

Fingerprinting of web traffic, that is, the identification of visits to websites, is a
challenging problem because of today's widespread use of encrypted communications. This
thesis work has demonstrated the effectiveness of using supervised learning techniques for
fingerprinting. In particular, the experimental results have shown that it is easy to
accurately identify web traffic even if encryption is in place. Moreover, despite the
different characteristics of the traffic associated with \acs{HTTP}/3 over QUIC, this work
has revealed that this traffic is not harder to fingerprint than traffic of older versions
of \ac{HTTP}.

% After the various challenges faced in the collection of a quality dataset...

Different approaches for feature selection were evaluated, with the best results obtained
with embedded methods. The trained weights of $L_1$-regularized \ac{SVM} and the Gini
feature importance assigned by Extra-Trees were good indicators of the quality of the
features.

Features extracted from the interpolated packet size cumulative sum are important for the
success of classification, especially for online classification, where the detection is
based on the first few packets. Incoming and outgoing packet ratios, as well as the size
of series of consecutive packets in the same direction are strong features when
classification is performed offline over the entire traffic.

Ensemble methods were able to obtain very good performance, with Random Forest being able
to achieve over $98\%$ accuracy in the classification of visits to 100 different websites.
For the real-world scenario \emph{k}-fingerprinting achieved a recall of up to $97.9\%$.
When considering online classification based on the first 100 packets, the recall is
similar, i.e., $97.2\%$.

% However, since a real use-case of this work could be for traffic monitoring over a network
% node, a separate study of the computational requirements of these algorithms is required
% before considering a possible deployment on embedded devices.

The fact that in real world traffic browsers make extensive use of caches, and that users
can choose among different web browsers were taken in consideration in the acquisition of
a dataset. It has been shown that a model trained on such diversified data is more robust
in front of these variations in classified traffic.

% To conclude, we have observed that the 5:1 ratio of monitored against unmonitored traffic
% samples used by this work is not enough to achieve the best possible performance. In fact,
% more traffic from unmonitored sites needs to be collected to achieve better precision and
% false positive rates than the ones hereby achieved.

% \section{Key Contributions}

% This is one of the first works~\cite{tong2018novel, smith2021website, zhan2021website}
% that took in consideration the possible influence of a mix of regular \acs{TLS} and QUIC
% traffic in the task of website classification. As far as we know, the same also holds true
% for the choice of using different web browsers and \acp{URL} when collecting data for each
% site as ways to increase variability in the dataset.

% The project described in this thesis followed an experimental approach in which data
% collection and cleaning were onerous processes and required particular care in the design.
% In particular, a software has been developed specifically for the task of coordinating the
% headless execution of the different web browsers with the traffic acquisition processes in
% an automated way. This tool is documented, highly configurable, portable and can be easily
% extended and used to do other research in this field.

% While various previous research was able to achieve isolation of the data acquisition
% processes by using Docker containers, what Docker uses under the hood is the same network
% namespaces approach proposed in this work. Using namespaces directly however allow for
% a leaner acquisition phase that can be easily extended to multiple devices.

% The \ac{DNS} analysis introduced in this work is a novel approach to remove
% browser-specific traffic from the acquired data. This approach will still be possible in
% the case \ac{DNS} encryption will take hold in the next years thanks to the availability
% of browser secrets in the training phase. However, when considering production
% deployments, it can be noted that the hostnames of unwanted services can change over
% browser updates, and that the only viable way to resolve these to IP addresses would be to
% continuously poll \ac{DNS} servers instead of analyzing existing cleartext \ac{DNS}
% requests as done in this work.

% Accurate even when cache is in use, discuss influence of use of caching on best features

% \section{Future Improvements}

% multivariate feature imputation

% extract TCP/UDP specific features
% evaluate agains tcp only same sites

Different research directions can be taken to extend and improve the model proposed in
this thesis work. For example, a possible direction refers to the investigation of the
effectiveness of the proposed approach in scenarios where users make use of privacy
enhancing technologies, like VPN tunnels or Tor.

% Previous research also developed different countermeasures for
% fingerprinting~\cite{hayes2016k, cai2012touching, wang2014effective} that might be subject
% of study in additional work.

% We can search for features that are stable over time in the face of changes and updates
% made to websites.

As a further research, deep architectures could be considered because these
models learn without a prior manual extraction and selection of features.
\aclp{CNN} have achieved good results in the field of image classification due to
their ability to exploit patterns in the data. Following the intuition that network
traffic could exhibit similar patterns, it could be beneficial to consider neural models.

The online classification problem, whose objective is to perform website classification in
real-time, could be further explored by varying the number of packets considered in each
traffic sample.

% Additionally, a system can be devised to use a pre-trained model to act upon traffic from
% multiple devices in a real-world scenario.

% Furthermore, also the used dataset can be improved: First, a 1:1 ratio among samples where
% the browser did or did not make use of the cache might not representative of real world
% traffic. A user might visit the same site multiple times, and the resulting traffic
% patterns can change between further visits due to the different expiration times of the
% loaded resources. Traffic samples can be collected from different operating systems to
% observe whether the \ac{TCP} and \ac{UDP} stacks inside the kernel, or platform
% differences in the browsers themselves might cause changes in traffic able to reduce
% classification performance when training and test data are originating from different
% OSes. While isolation of acquisition processes is possible in Microsoft Windows, it
% requires tracking of the different IDs of the multiple processes spawned by modern
% browsers, requiring a different approach than the one used in Linux to traffic
% preprocessing.

% more detailed post capture anomaly detection

% To conclude, it has to be noted that this work, like most of the previous work, did not
% consider the changes in traffic that could be observed when visiting dynamic
% websites~\cite{oh2017p, bhat2019var}. Examples of these resources can be web-based email
% or social feeds, where the content has a high probability to change depending on the
% logged-in user. This is a possible direction for further research in website
% fingerprinting.

\vspace*{20pt}

\begingroup
\sffamily \small All the software developed as part of this thesis work, \\
as well as the source of this document are freely available at: \\[2mm]
\large\url{https://luca.zanus.si/unipv/fingerprinting/}
\endgroup
