.DEFAULT_GOAL := cbar_pdf

docname := thesis
tempdir := ./latexbuild
latexparams := -halt-on-error -shell-escape -interaction=nonstopmode -synctex=1 -output-directory=$(tempdir)
tex_files := $(wildcard ./*.tex ./**/*.tex)

$(tempdir)/$(docname).bcf: $(docname).tex
	mkdir -p $(tempdir)
	pdflatex $(latexparams) $(docname).tex || ( cp $(tempdir)/$(docname).log . && $(MAKE) clean )

$(tempdir)/$(docname).bbl: $(tempdir)/$(docname).bcf
	biber $(tempdir)/$(docname).bcf
	makeglossaries -d $(tempdir) $(docname)

$(tempdir)/$(docname).pdf: $(docname).tex $(tempdir)/$(docname).bbl
	pdflatex $(latexparams) $(docname).tex || ( cp $(tempdir)/$(docname).log . && $(MAKE) clean )
	pdflatex $(latexparams) $(docname).tex

pdf: $(tempdir)/$(docname).pdf
	cp $(tempdir)/$(docname).pdf $(docname).pdf
	cp $(tempdir)/$(docname).synctex.gz $(docname).synctex.gz

cbar_pdf: $(tempdir)/$(docname).pdf
	# More builds for changebar
	pdflatex $(latexparams) $(docname).tex
	pdflatex $(latexparams) $(docname).tex
	cp $(tempdir)/$(docname).pdf $(docname).pdf
	cp $(tempdir)/$(docname).synctex.gz $(docname).synctex.gz

#spellcheck:
#	find . -name '*.tex' -exec aspell -p $(shell realpath aspell_dict) -c {} \;

diff:
	git latexdiff --main $(docname).tex --type=CULINECHBAR $(filter-out $@,$(MAKECMDGOALS))

.PHONY: clean
clean:
	rm -rf $(tempdir)
